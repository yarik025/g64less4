//
//  ViewController.swift
//  Less4
//
//  Created by Yaroslav Georgievich on 16.07.2018.
//  Copyright © 2018 Yaroslav Georgievich. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //sayHello()
        //whatDay(day: 7)
        //sumTwoNumber(fistNumber: 2, secondNumber: 3)
        //print(resultNumber(number: 2.5))
        //printHello(numberSay: 5)
        //fib(number: 5)
    }
    //1
    func sayHello() {
        print("Hello!:)")
    }
    //2
    func whatDay(day: Int) {
        if day == 1 {
            print("Сегодня Понедельник")
        }
        else if day == 2 {
            print("Сегодня Вторник")
        }
        else if day == 3 {
            print("Сегодня Среда")
        }
        else if day == 4 {
            print("Сегодня Четверг")
        }
        else if day == 5 {
            print("Сегодня Пятница")
        }
        else if day == 6 {
            print("Сегодня Суббота")
        }
        else if day == 7 {
            print("Сегодня Восскресенье")
        }
        else {
            print("Нету такого дня недели")
        }
    }
    //3
    func sumTwoNumber(fistNumber: Int, secondNumber: Int) {
        let sumNumber = fistNumber + secondNumber
        print("Сумма равна: ", sumNumber)
    }
    //4
    func resultNumber(number: Double) -> Double {
        let result = number * 30 + 5
        return result
    }
    //5
    func printHello(numberSay: Int) {
        for _ in 0..<numberSay {
            print("Hello")
        }
    }
    //6
//    func fib(number: Int) {
//        let a = 0
//        let b = 1
//        var c = 0
//        var d = 0
//        for _ in 0..<number {
//            c = c + a + b
//            d = c + b
//            print(d)
//        }
//    }
}

